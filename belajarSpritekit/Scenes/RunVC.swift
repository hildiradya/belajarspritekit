//
//  RunVC.swift
//  belajarSpritekit
//
//  Created by Hildi Radya on 21/03/21.
//  Copyright © 2021 Hildi Radya. All rights reserved.
//

import GameplayKit

class RunVC: SKScene {

    private lazy var buttonLeft: SKShapeNode = {
        var node = SKShapeNode(rectOf: CGSize(width: 100, height: 100))
        node.name = "leftBar"
        node.fillColor = SKColor.white
        return node
    }()

    private lazy var buttonRight: SKShapeNode = {
        var node = SKShapeNode(rectOf: CGSize(width: 100, height: 100))
        node.name = "rightBar"
        node.fillColor = SKColor.white
        return node
    }()

    var gameTimer: Timer?
    let girl: Girl
    let obstacle: Debris
    var mainCharacterCgpoint = CGPoint()
    var leftBtnCgpoint = CGPoint()
    var rightBtnCgpoint = CGPoint()
    /// Y
    let spawnStandard: CGFloat = 12
    let bottomStandard: CGFloat = 2
    let underScreen: CGFloat = -1
    /// X
    let leftStandard: CGFloat = 3
    let middleStandard: CGFloat = 8
    let rightStandard: CGFloat = 13
    
    var lastUpdateTime: TimeInterval = 0.0
    var dt: TimeInterval = 0.0
    var isTime: CGFloat = 3.0

    override init(size: CGSize) {
        girl = Girl()
        obstacle = Debris()

        super.init(size: size)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let touchPosition = touch.location(in: self)
        let touchedNodes = nodes(at: touchPosition)
        for node in touchedNodes {
            if let _ = node as? SKShapeNode, node.name == "leftBar" {
                if self.girl.pointCharacter == .mid {/// to left
                    toLeft()
                } else if self.girl.pointCharacter == .right { ///to mid
                    toMid()
                }
            }

            if let _ = node as? SKShapeNode, node.name == "rightBar" {
                if self.girl.pointCharacter == .mid { ///to right
                    toRight()
                } else if self.girl.pointCharacter == .left { ///to mid
                    toMid()
                }
            }
        }
    }
    
//    override func update(_ currentTime: TimeInterval) {
//        if lastUpdateTime > 0 {
//            dt = currentTime - lastUpdateTime
//        } else {
//            dt = 0
//        }
//
//        print(currentTime)
//    }
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        setup()
    }
    
    private func toMid() {
        let pointY = frame.maxY / 16
        let pointX = frame.maxX / 16
        let leftPoint = CGPoint(x: pointX * middleStandard, y: pointY * bottomStandard)
        girl.swipe(cgPoint: leftPoint)
        girl.pointCharacter = .mid
    }

    private func toLeft() {
        let pointY = frame.maxY / 16
        let pointX = frame.maxX / 16
        let leftPoint = CGPoint(x: pointX * leftStandard, y: pointY * bottomStandard)
        girl.swipe(cgPoint: leftPoint)
        girl.pointCharacter = .left
    }

    private func toRight() {
        let pointY = frame.maxY / 16
        let pointX = frame.maxX / 16
        let leftPoint = CGPoint(x: pointX * rightStandard, y: pointY * bottomStandard)
        girl.swipe(cgPoint: leftPoint)
        girl.pointCharacter = .right
    }

    private func setup() {
        let pointY = frame.maxY / 16
        let pointX = frame.maxX / 16
        let leftUpper = CGPoint(x: pointX * 4, y: pointY * 12)
        let midUpper = CGPoint(x: pointX * 8, y: pointY * 12)
        let rightUpper = CGPoint(x: pointX * 12, y: pointY * 12)

        let leftMid = CGPoint(x: pointX * 4, y: pointY * 8)
        let midMid = CGPoint(x: pointX * 8 , y: pointY * 8)
        let rightMid = CGPoint(x: pointX * 12, y: pointY * 8)

        let leftBelow = CGPoint(x: pointX * 4, y: pointY * 4)
        let midBelow = CGPoint(x: pointX * 8, y: pointY * 4)
        let rightBelow = CGPoint(x: pointX * 12, y: pointY * 4)

        startGame()

        /// button position
        rightBtnCgpoint = CGPoint(x: pointX * rightStandard, y: pointY * 15)
//        rightBtnCgpoint = CGPoint(x: pointX * 8 , y: pointY * 8)
        leftBtnCgpoint = CGPoint(x: pointX * leftStandard, y: pointY * 15)
        buttonLeft.position = leftBtnCgpoint
        buttonRight.position = rightBtnCgpoint
        addChild(buttonLeft)
        addChild(buttonRight)
        /// main character position
        mainCharacterCgpoint = CGPoint(x: pointX * middleStandard, y: pointY * bottomStandard)
        
        girl.physicsBody = SKPhysicsBody(rectangleOf: girl.size)
        girl.physicsBody!.isDynamic = false
        girl.physicsBody!.categoryBitMask = PhysicsCategory.Player
        girl.physicsBody!.contactTestBitMask = PhysicsCategory.Obstacle
        addChild(girl)
        girl.setPosition(cgPoint: mainCharacterCgpoint)
        
    }

    private func spawnDebris() { /// asal spawn mengikuti
        let obstacle = Debris()
        obstacle.setScale(0.3)
        let pointY = frame.maxY / 16
        let pointX = frame.maxX / 16
        
        obstacle.physicsBody = SKPhysicsBody(rectangleOf: obstacle.size)
        obstacle.physicsBody!.isDynamic = false
        obstacle.physicsBody!.categoryBitMask = PhysicsCategory.Obstacle
        obstacle.physicsBody!.contactTestBitMask = PhysicsCategory.Player
        obstacle.setPosition(cgPoint: CGPoint(x: pointX * leftStandard, y: pointY))
        addChild(obstacle)
        
        let arah = Int.random(in: 1...3)
        if arah == 1 {///left
            let midUpper = CGPoint(x: pointX * 6, y: pointY * spawnStandard)/// 6 adalah agak ke kiri
            let leftBelow = CGPoint(x: pointX * leftStandard, y: pointY * underScreen)
            obstacle.setPosition(cgPoint: midUpper)
            obstacle.comeCloser(cgPoint: leftBelow)
        } else if arah == 2 {///mid
            let midUpper = CGPoint(x: pointX * middleStandard, y: pointY * spawnStandard) /// tengah adalah 8
            let midBelow = CGPoint(x: pointX * middleStandard, y: pointY * underScreen)
            obstacle.setPosition(cgPoint: midUpper)
            obstacle.comeCloser(cgPoint: midBelow)
        } else {///right
            let midUpper = CGPoint(x: pointX * 10, y: pointY * spawnStandard) /// 10 adalah agk ke kanan
            let rightBelow = CGPoint(x: pointX * rightStandard, y: pointY * underScreen)
            obstacle.setPosition(cgPoint: midUpper)
            obstacle.comeCloser(cgPoint: rightBelow)
        }
    }

    private func spawnDebrisMid() { ///asal spawn slalu tengah
        let obstacle = Debris()
        let pointY = frame.maxY / 16
        let pointX = frame.maxX / 16
        let midUpper = CGPoint(x: pointX * middleStandard, y: pointY * 12)
        let rightBelow = CGPoint(x: pointX * 18, y: pointY * 0)
        let leftBelow = CGPoint(x: pointX * 0, y: pointY * 0)
        addChild(obstacle)
        let arah = Int.random(in: 1...2)
        if arah == 1 {
            obstacle.setPosition(cgPoint: midUpper)
            obstacle.comeCloser(cgPoint: rightBelow)
        } else {
            obstacle.setPosition(cgPoint: midUpper)
            obstacle.comeCloser(cgPoint: leftBelow)
        }
    }

    private func startGame() {
        gameTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(Int.random(in: 3...5)), repeats: true, block: {_ in
            self.spawnDebris()
        })
    }
}

// MARK: - Physics Contact Delegate

extension RunVC : SKPhysicsContactDelegate {

    func didBegin(_ contact: SKPhysicsContact) {
        print("Contact")

        let other = contact.bodyA.categoryBitMask == PhysicsCategory.Player ? contact.bodyB : contact.bodyA

        switch other.categoryBitMask {
            
        case PhysicsCategory.Obstacle:
            print("Obstacle")
        default:
//            print("None")
            break
        }

    }
}
