//
//  DimasScene.swift
//  belajarSpritekit
//
//  Created by Hildi Radya on 22/03/21.
//  Copyright © 2021 Hildi Radya. All rights reserved.
//

import SpriteKit
import GameplayKit

class DimasScene: SKScene, SKPhysicsContactDelegate {
    
    //Properties
    var player: SKSpriteNode!
    var debris: SKSpriteNode!
    
    var buttonLeft: SKShapeNode!
    var buttonRight: SKShapeNode!
    var buttonJump: SKShapeNode!
    
    var textures: [SKTexture] = []

    var playerPosZ: CGFloat = 0.0
    var velocityY: CGFloat = 0.0
    var dy: CGFloat = 0.0
//    var body: SKPhysicsBody!
    var gameTimer: Timer?
    var onGround = true
        
    //System
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let touchPosition = touch.location(in: self)
        let touchedNodes = nodes(at: touchPosition)
        for node in touchedNodes {
            
            if let _ = node as? SKShapeNode, node.name == "Left" {
//                print("Left")
                player.position.x -= 30
            }
            
            if let _ = node as? SKShapeNode, node.name == "Right" {
//                print("Right")
                player.position.x += 30
            }
            
            if let _ = node as? SKShapeNode, node.name == "Jump" {
//                print("Jump")
                if let body = player.physicsBody {
                    dy = body.velocity.dy
//                    print(dy)
                    playerJump()
//                    if dy > 0 && !onGround {
//                        body.collisionBitMask &= ~PhysicsCategory.Player
//                        onGround = true
//                    } else {
//                        body.collisionBitMask |= PhysicsCategory.Player
//                    }
                }
            }
                
        }
    }
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        setupGirl()
        setupDebris()
        spawnDebris()
        setupButton()
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        if player.position.y > 10.0 {

//            player.position.y = 0.0
//            velocityY = 0.0
//            onGround = true
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let other = contact.bodyA.categoryBitMask == PhysicsCategory.Player ? contact.bodyB : contact.bodyA
        
        switch other.categoryBitMask {
        case PhysicsCategory.Obstacle:
            print("Ops")
//            player.zRotation(0)
            other.node?.removeFromParent()
        default:
            break
        }

    }
    
}

//Configuration
extension DimasScene {
    
    func setupGirl() {
        player = SKSpriteNode(imageNamed: "coin-1")
        player.name = "Player"
        player.setScale(0.5)
        player.zPosition = 10.0
        player.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        player.position = CGPoint(x: frame.width / 2.0, y: 0.0)
        player.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: player.frame.width, height: player.frame.height/2.0), center: player.anchorPoint)
//        player.physicsBody!.isDynamic = false
        player.physicsBody!.allowsRotation = false
        player.physicsBody!.angularVelocity = 0
        player.physicsBody!.angularDamping = 0
//        player.physicsBody!.pinned = true
        player.physicsBody!.affectedByGravity = false
        player.physicsBody!.categoryBitMask = PhysicsCategory.Player
        player.physicsBody!.contactTestBitMask = PhysicsCategory.Obstacle
//        player.physicsBody!.collisionBitMask
        playerPosZ = player.zPosition
        addChild(player)
        
        for i in 1...6 {
            textures.append(SKTexture(imageNamed: "coin-\(i)"))
        }
        
        player.run(.repeatForever(.animate(with: textures, timePerFrame: 0.083)))
    }
    
    func playerJump() {
        let previousPosition = player.position
        onGround = false
        var texturesJump: [SKTexture] = []
        for i in 1...3 {
            texturesJump.append(SKTexture(imageNamed: "block-\(i)"))
        }
        
        let jumpUp = SKAction.moveBy(x: 0, y: 100, duration: 1.0)
        let jumpDown = SKAction.moveBy(x: 0, y: -100, duration: 1.0)
        let jumpSequence = SKAction.sequence([jumpUp, jumpDown])
//        let jumpRemove = SKAction.removeFromParent()
        let jumpAnimate = SKAction.animate(with: texturesJump, timePerFrame: 0.666)
        let collisionJump = SKAction.run {
            self.playerCollisionJump()
        }
        let groupJump = SKAction.group([jumpSequence, jumpAnimate, collisionJump])
        
        self.player.run(.sequence([groupJump]), completion: {
            self.player.run(.repeatForever(.animate(with: self.textures, timePerFrame: 0.083)))
//            self.debris.physicsBody!.isDynamic = true
            self.player.physicsBody!.categoryBitMask = PhysicsCategory.Player
            if self.player.position != previousPosition {
                self.player.run(SKAction.moveTo(y: 0, duration: 0.1))
            }
//            self.player.physicsBody?.contactTestBitMask = PhysicsCategory.Obstacle
        })
        
        
        
//        player.physicsBody?.applyForce(CGVector(dx: 0, dy: 50))
//        player.run(.repeatForever(.animate(with: texturesJump, timePerFrame: 0.083)))

    }
    
    func playerCollisionJump() {
        print("disable collision box")
//        self.debris.physicsBody!.isDynamic = false
        self.player.physicsBody!.categoryBitMask = PhysicsCategory.Block
//        self.player.physicsBody?.contactTestBitMask = PhysicsCategory.Block

    }
    
    func setupDebris() {
        debris = SKSpriteNode(imageNamed: "coin-1")
        debris.name = "Debris"
        debris.setScale(0.1)
        debris.zPosition = 5.0
        debris.position = CGPoint(x: frame.width / 2.0, y: frame.height)
        debris.physicsBody = SKPhysicsBody(rectangleOf: debris.size)
//        debris.physicsBody = SKPhysicsBody(polygonFrom: CGPath()
//        debris.physicsBody!.isDynamic = false
//        debris.physicsBody!.allowsRotation = false
//        debris.physicsBody!.angularVelocity = 0
        debris.physicsBody!.affectedByGravity = false
        debris.physicsBody!.categoryBitMask = PhysicsCategory.Obstacle
        debris.physicsBody!.contactTestBitMask = PhysicsCategory.Player
        addChild(debris)
        
        let arah = Int.random(in: 1...3)
        let moveMid: SKAction!
        let moveLowLeft: SKAction!
        let moveLowRight: SKAction!
        if arah == 1 {
            moveMid = SKAction.move(to: CGPoint(x: frame.width/2.0, y: -200.0), duration: 4)
            let scaling = SKAction.scale(to: 1.0, duration: 5)
            let remove = SKAction.removeFromParent()
            let group = SKAction.group([scaling, moveMid])
            debris.run(.sequence([group, remove]))

        } else if arah == 2 {
            moveLowLeft = SKAction.move(to: CGPoint(x: 0.0, y: -200.0), duration: 4)
            let scaling = SKAction.scale(to: 1.0, duration: 5)
            let remove = SKAction.removeFromParent()
            let group = SKAction.group([scaling, moveLowLeft])
            debris.run(.sequence([group, remove]))

        } else if arah == 3 {
            moveLowRight = SKAction.move(to: CGPoint(x: frame.width, y: -200.0), duration: 4)
            let scaling = SKAction.scale(to: 1.0, duration: 5)
            let remove = SKAction.removeFromParent()
            let group = SKAction.group([scaling, moveLowRight])
            debris.run(.sequence([group, remove]))

        }
        
    }
    
    func spawnDebris() {
        let random = Double(CGFloat.random(min: 1.5, max: 3.0))
        run(.repeatForever(.sequence([
            .wait(forDuration: random),
            .run { [weak self] in
                self?.setupDebris()
            }
        ])))
    }
    
    func setupButton() {
        buttonLeft = SKShapeNode(rectOf: CGSize(width: 100, height: 100))
        buttonRight = SKShapeNode(rectOf: CGSize(width: 100, height: 100))
        buttonJump = SKShapeNode(rectOf: CGSize(width: 100, height: 100))
        
        buttonLeft.fillColor = .white
        buttonRight.fillColor = .white
        buttonJump.fillColor = .white

        buttonLeft.position = CGPoint(x: 0.0, y: frame.height)
        buttonRight.position = CGPoint(x: frame.width, y: frame.height)
        buttonJump.position = CGPoint(x: frame.width / 2.0, y: frame.height)
        
        buttonLeft.name = "Left"
        buttonRight.name = "Right"
        buttonJump.name = "Jump"
        
        addChild(buttonLeft)
        addChild(buttonRight)
        addChild(buttonJump)
    }
}
