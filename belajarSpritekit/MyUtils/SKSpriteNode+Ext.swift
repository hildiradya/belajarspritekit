//
//  SKSpriteNode+Ext.swift
//  belajarSpritekit
//
//  Created by Hildi Radya on 21/03/21.
//  Copyright © 2021 Hildi Radya. All rights reserved.
//

import GameplayKit

extension SKSpriteNode {
    func setPosition(x:CGFloat, y: CGFloat) {
        self.position.x = x
        self.position.y = y
    }

    func setPosition(cgPoint: CGPoint) {
        self.position = cgPoint
    }
}
