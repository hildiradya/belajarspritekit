//
//  CGFloat+Ext.swift
//  belajarSpritekit
//
//  Created by Hildi Radya on 13/02/21.
//  Copyright © 2021 Hildi Radya. All rights reserved.
//

import CoreGraphics

public let n = CGFloat.pi

extension CGFloat {
    
    func radiansToDegree() -> CGFloat {
        return self * 180.0 / n
    }
    
    func degreeToRadians() -> CGFloat {
        return self * n / 180.0
    }
    
    //return 0 or 1
    static func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / Float(0xFFFFFFFF))
    }
    
    //return min or max
    static func random(min: CGFloat, max: CGFloat) -> CGFloat {
        assert(min < max)
        return CGFloat.random() * (max - min) + min
    }
}
