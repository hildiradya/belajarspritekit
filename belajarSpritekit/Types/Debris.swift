//
//  Debris.swift
//  belajarSpritekit
//
//  Created by Hildi Radya on 21/03/21.
//  Copyright © 2021 Hildi Radya. All rights reserved.
//

import SpriteKit

class Debris: SKSpriteNode {
    init() {
        let size = CGSize(width: 60, height: 60)
        let texture = SKTexture(imageNamed: "ninja")
        super.init(texture: texture, color: UIColor.clear, size: size)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        name = "Debris"
        zPosition = 5.0
        physicsBody = SKPhysicsBody(rectangleOf: size)
        physicsBody!.affectedByGravity = false
        physicsBody!.isDynamic = false
        physicsBody!.categoryBitMask = PhysicsCategory.Obstacle
        physicsBody!.contactTestBitMask = PhysicsCategory.Player

    }

    func comeCloser(cgPoint: CGPoint) {
        let move = SKAction.move(to: cgPoint, duration: 4)
        let scaling = SKAction.scale(to: 1.0, duration: 5)
        let remove = SKAction.removeFromParent()
        let group = SKAction.group([scaling, move])
        let sequenceAction = SKAction.sequence([group, remove])
        self.run(sequenceAction)
    }

    func comingCloser() {
        //        ukuran dari kecil-membesar sampai scale 1.0
        //        gambar dari atas-turun ke bawah sampai titik tertentu
    }
}

extension Debris : SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let other = contact.bodyA.categoryBitMask == PhysicsCategory.Obstacle ? contact.bodyB : contact.bodyA
        
        switch other.categoryBitMask {
            
        case PhysicsCategory.Player:
            print("Hit")
        default:
            break
        }

    }
}

