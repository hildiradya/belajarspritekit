//
//  Girl.swift
//  belajarSpritekit
//
//  Created by Hildi Radya on 21/03/21.
//  Copyright © 2021 Hildi Radya. All rights reserved.
//

import SpriteKit

class Girl: SKSpriteNode {

    enum PointCharacter {
        case left
        case mid
        case right
    }

    var pointCharacter: PointCharacter = .mid

    init() {
        let size = CGSize(width: 60, height: 100)
        let texture = SKTexture(imageNamed: "ninja")
        super.init(texture: texture, color: UIColor.clear, size: size)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        name = "NamaGirl"
        zPosition = 5.0
        physicsBody = SKPhysicsBody(rectangleOf: size)
        physicsBody!.affectedByGravity = false
        physicsBody!.isDynamic = false
        physicsBody!.categoryBitMask = PhysicsCategory.Player
        physicsBody!.contactTestBitMask = PhysicsCategory.Obstacle

    }
    
    func swipe(cgPoint: CGPoint) {
        let move = SKAction.move(to: cgPoint, duration: 0.3)
        //SKAction.moveBy(x: -200, y: 0, duration: 0.2)
        self.run(move)
    }
}

extension Girl : SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let other = contact.bodyA.node?.name == "Girl" ? contact.bodyB : contact.bodyA
        
        switch other.node?.name {
            
        case "Debris":
            print("Hit")
        default:
            break
        }

    }
}
