//
//  GameViewController.swift
//  belajarSpritekit
//
//  Created by Hildi Radya on 07/02/21.
//  Copyright © 2021 Hildi Radya. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let scene = RunVC(size: CGSize(width: 2048, height: 1536))
        let scene = DimasScene(size: view.frame.size)
        scene.scaleMode = .aspectFill
        
        let skView = view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.showsPhysics = true
        skView.presentScene(scene)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
